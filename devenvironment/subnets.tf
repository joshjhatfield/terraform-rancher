

# firewall UTM subnet public
resource "aws_subnet" "public_subnet-az1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "${var.public_az1}"
    availability_zone = "${var.aws_az1}"

    tags {
        Name = "${var.prefix}-public-az1"
    }
}


# firewall UTM subnet private
resource "aws_subnet" "utm_subnet-az1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "${var.utm_az1}"
    availability_zone = "${var.aws_az1}"

    tags {
        Name = "${var.prefix}-utm-az1"
    }
}


# rancher subnet
resource "aws_subnet" "rancher_subnet-az1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "${var.rancher_az1}"
    availability_zone = "${var.aws_az1}"

    tags {
        Name = "${var.prefix}-rancher-az1"
    }
}



# app subnet
resource "aws_subnet" "app_subnet-az1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "${var.app_az1}"
    availability_zone = "${var.aws_az1}"

    tags {
        Name = "${var.prefix}-app-az1"
    }
}




# Core server internal subnet
resource "aws_subnet" "core_subnet-az1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "${var.core_az1}"
    availability_zone = "${var.aws_az1}"

    tags {
        Name = "${var.prefix}-core-az1"
    }
}

