# backend SG
resource "aws_security_group" "backend" {
  name = "${var.coreaccesssg}"
  description ="main access SG"
  name = "${var.prefix}-backend"
  vpc_id = "${aws_vpc.main.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  ingress {
    from_port = 8140
    to_port = 8140
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]

  egress {
    from_port = 8140
    to_port = 8140
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }
   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 53
    to_port = 53
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 389
    to_port = 389
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
}



# app access

resource "aws_security_group" "appaccess" {
  name = "${var.prefix}-app"
  description ="Access to the app servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  ingress {
    from_port = 10101
    to_port = 10150
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  egress {
    from_port = 10101
    to_port = 10150
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  egress {
    from_port = 5672
    to_port = 5672
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
  egress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
}


# core access

resource "aws_security_group" "coreaccess" {
  name = "${var.prefix}-app"
  description ="Access to the core servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  egress {
    from_port = 5672
    to_port = 5672
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
  egress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
}




# Private UTM access

resource "aws_security_group" "utmaccess" {
  name = "${var.prefix}-app"
  description ="Access to the private utm servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  ingress {
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
  egress {
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["${var.vpc_cidr}"]  
  }
}


resource "aws_security_group" "pubaccess" {
  name = "${var.prefix}-app"
  description ="Access for public firewalls"
  vpc_id = "${aws_vpc.main-jh.id}"
  # Change after build
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  # Change after build
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  egress {
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]  
  }
}