resource "aws_instance" "az1app" {
  ami = "${var.ubnt-ami}"
  instance_type = "${var.awsr3large}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.appaccess.id}", "${aws_security_group.backend.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.pp_subnet-az1.id}"

  root_block_device {
    volume_type = "standard"
    volume_size = "${var.ec2rancherserver}"
    delete_on_termination = "true"
  }

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "standard"
    volume_size = "${var.ec2varlog}"
    delete_on_termination = "true"
  }

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.az1}${var.prefix}-app${count.index + 1}
              sudo echo "${var.az1}${var.prefix}-app${count.index + 1}.${var.privdns}" > /etc/hostname
              sudo echo "127.0.0.1 ${var.az1}${var.prefix}-app${count.index + 1}.${var.privdns}" >> /etc/hosts
              hostname -F /etc/hostname
              sudo systemctl restart systemd-logind.service
              EOF

  tags {
    Name = "${var.az1}${var.prefix}-app${count.index + 1}"
    Environment = "${var.az1}${var.prefix}"
    ServerType = "app"
  }
}