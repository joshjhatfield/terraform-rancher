variable "aws_access_key" {}
variable "aws_secret_key" {}


########################
## AWS GLOBAL SETTINGS##
########################

##### VPC details
###-----------------------------------
# VPC name
variable "vpcname" {
  description = "describe your variable"
  default = "jhtest"
}

# IAM key name - pick an existing server key
variable "iamkey" {
  default ="terraformlk"
  description = "name of IAM key being used"
}

### --------------------------------

# The Global server prefix
variable "prefix" {
  default ="jh"
  description = "the global prefix for this build"
}

# naming servers by AZ
variable "az1" {
  default ="az1"
  description = "Availability zone 1 prefix"
}

variable "az2" {
  default ="az2"
  description = "Availability zone 2 prefix"
}

# The private DNS record
variable "privdns" {
  default = "aws.jhdev.com"
  description = "The global private dns record for route53"
}

# AWS region
variable "aws_region" {
    description = "aws region to use"
    default = "us-west-2"
}

# AWS Availability zones
variable "aws_az1" {
    description = "az1 Availability zones"
    default = "us-west-2a"
}

variable "aws_az2" {
    description = "az2 Availability zones"
    default = "us-west-2b"
}



#############################
## Server Config ##
#############################
## AMI'S
# Ubuntu AMI
variable "ubnt-ami" {
  description= "Ubuntu 16.04 ami"
  default= "ami-a58d0dc5"
}

## Machine sizes
variable "awst2micro" {
  description= "t2.micro AMI size"
  default= "t2.micro"
}

variable "awsm3medium" {
  description= "m3.medium instance size 1xcpu, 4GB ram"
  default= "m3.medium"
}

variable "awsm4large" {
  description= "m4.large instance size 2xcpu, 8GB ram"
  default= "m4.large"
}

variable "awsr3large" {
  description= "r3.large instance size 2xcpu, 15GB ram mem oprimized"
  default= "r3.large"
}

# EBS Volume sizes

variable "ec2varlog" {
  description= "the size of /var/log in GB"
  default= "10"
}

variable "ec2libelastic" {
  description= "the size of /lib/elasticsearch in GB"
  default= "30"
}

variable "ec2rancherserver" {
  description= "the size of /lib/elasticsearch in GB"
  default= "60"
}


#######################
##  VPC AND SUBNETS  ##
#######################
# network subnets and vpc
variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.83.0.0/16"
}


# rancher server
variable "rancher_az1" {
  description = "the subnet for rancher servers"
  default = "10.83.5.0/24"
}


# Core subnets
variable "core_az1" {
    description = "CIDR for sy1 internal core subnet"
    default = "10.83.10.0/24"
}


# app
variable "app_az1" {
    description = "CIDR for sy1 app subnet"
    default = "10.83.15.0/24"
}



## Firewall subs

# public 
variable "public_az1" {
    description = "CIDR for public subnet"
    default = "10.83.80.0/24"
}

# private 
variable "putm_az1" {
    description = "CIDR for private subnet"
    default = "10.83.82.0/24"
}


