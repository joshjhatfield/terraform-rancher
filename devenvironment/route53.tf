resource "aws_route53_zone" "privdns" {
  name = "${var.privdns}"
  vpc_id = "${aws_vpc.main.id}"
  force_destroy = "true"
  tags {
  	Name = "${var.privdns}"
  }
}

###############
## A RECORDS ##
###############

# rancher server records
resource "aws_route53_record" "rancheradns" {
 zone_id = "${aws_route53_zone.privdns.zone_id}"
 name = "${var.az1}${var.prefix}-rancher${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1rancher.private_ip}"]
}

# app server records
resource "aws_route53_record" "appadns" {
 zone_id = "${aws_route53_zone.privdns.zone_id}"
 name = "${var.az1}${var.prefix}-app${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1app.private_ip}"]
}

# core server records
resource "aws_route53_record" "coreadns" {
 zone_id = "${aws_route53_zone.privdns.zone_id}"
 name = "${var.az1}${var.prefix}-core${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1core.private_ip}"]
}