resource "aws_vpc" "main" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_support = "true"
    enable_dns_hostnames ="true"
      tags {
    Name = "${var.vpcname}"
  }
}

# internet gateway
resource "aws_internet_gateway" "ig-gw" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "${var.vpcname}"
  }
}

# dhcp options

resource "aws_vpc_dhcp_options" "osdnsopts" {
    domain_name = "${var.privdns}"
    domain_name_servers= ["AmazonProvidedDNS"]
    tags {
    Name = "${var.vpcname}"
  }
}

    
resource "aws_vpc_dhcp_options_association" "osdns_reolver" {
  vpc_id          = "${aws_vpc.main.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.osdnsopts.id}"
}


# the Elastic IPs in to the network

# Bastion
resource "aws_eip" "bastion_eip" {
  vpc = true
  instance = "${aws_instance.az1bastion.id}"
}

