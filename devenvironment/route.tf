## Internal routes ##



# UTM public

resource "aws_route_table" "az1-public-route" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.ig-gw.id}"
    }

    tags {
        Name = "${var.az1}${var.prefix}-public-route"
    }
}

resource "aws_route_table_association" "aws_az1-public" {
    subnet_id = "${aws_subnet.public_subnet-az1.id}"
    route_table_id = "${aws_route_table.az1-public-route.id}"
}



# UTM private

resource "aws_route_table" "az1-utm-route" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.ig-gw.id}"
    }

    tags {
        Name = "${var.az1}${var.prefix}-utm-route"
    }
}

resource "aws_route_table_association" "aws_az1-utm" {
    subnet_id = "${aws_subnet.utm_subnet-az1.id}"
    route_table_id = "${aws_route_table.az1-utm-route.id}"
}


# rancher route table 
resource "aws_route_table" "az1-rancher-route" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.ig-gw.id}"
    }

    tags {
        Name = "${var.az1}${var.prefix}-rancher-route"
    }
}

resource "aws_route_table_association" "aws_az1-rancher" {
    subnet_id = "${aws_subnet.rancher_subnet-az1.id}"
    route_table_id = "${aws_route_table.az1-rancher-route.id}"
}




# app route table 
resource "aws_route_table" "az1-app-route" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.ig-gw.id}"
    }

    tags {
        Name = "${var.az1}${var.prefix}-app-route"
    }
}

resource "aws_route_table_association" "aws_az1-app" {
    subnet_id = "${aws_subnet.app_subnet-az1.id}"
    route_table_id = "${aws_route_table.az1-app-route.id}"
}


# core route table - goes to firewall - change soon
resource "aws_route_table" "az1-core-route" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.ig-gw.id}"
    }

    tags {
        Name = "${var.az1}${var.prefix}-core-route"
    }
}

resource "aws_route_table_association" "aws_az1-core" {
    subnet_id = "${aws_subnet.core_subnet-az1.id}"
    route_table_id = "${aws_route_table.az1-core-route.id}"
}